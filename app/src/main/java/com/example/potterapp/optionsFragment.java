package com.example.potterapp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class optionsFragment extends Fragment {

    @BindView(R.id.house_textView)
    TextView houseTextView;
    @BindView(R.id.my_characters_btn)
    Button myCharactersBtn;
    private OptionsViewModel mViewModel;

    public static optionsFragment newInstance() {
        return new optionsFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.options_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(OptionsViewModel.class);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @OnClick(R.id.sorting_hat_btn)
    public void onViewClicked() {
        LiveData<String> houseLiveData = mViewModel.getSortingHat();
        houseLiveData.observe(this, this::onHouseResponse);

    }

    private void onHouseResponse(String house) {
        houseTextView.setText(house);
        //myCharactersBtn.setText(R.string.characters_from_my_house);
        myCharactersBtn.setVisibility(Button.VISIBLE);
    }

    @OnClick(R.id.all_characters_btn)
    public void onAllCharactersBtnClicked(View view) {
        Navigation.findNavController(view).navigate(R.id.toCharacterList);
    }

    @OnClick(R.id.my_characters_btn)
    public void onMyCharactersBtnClicked() {
    }
}
