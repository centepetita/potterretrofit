package com.example.potterapp;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.example.potterapp.repository.PotterRepository;

public class OptionsViewModel extends AndroidViewModel {
        PotterRepository repository;


    public OptionsViewModel(@NonNull Application application) {
        super(application);
        repository = new PotterRepository(application);
    }

    public LiveData<String> getSortingHat() {
        return repository.getSortingHat();
    }
}
