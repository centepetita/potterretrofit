package com.example.potterapp.repository;

import android.app.Application;
import android.util.Log;
import android.widget.Toast;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.potterapp.model.PotterCharacter;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PotterRepository {



    public static final String API_BASE_URL = "https://www.potterapi.com";
    public static final String KEY="$2a$10$pFanOb26cOYGoxdOWkmMee9ij4TsZpcN3E0xsTyxo4DaOeq4eT2NC";
    PotterService potterService;
    Application application;
    MutableLiveData<List<PotterCharacter>> potters = new MutableLiveData<List<PotterCharacter>>();
    MutableLiveData<String> house=new MutableLiveData<>();

    public PotterRepository(Application application) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API_BASE_URL).addConverterFactory(GsonConverterFactory.create())
                .build();

        potterService = retrofit.create(PotterService.class);
        this.application = application;
    }

    public LiveData<List<PotterCharacter>> getPotterCharacters() {
        Call<List<PotterCharacter>> call = potterService.listCharacters(KEY);
        call.enqueue(new Callback<List<PotterCharacter>>() {
            @Override
            public void onResponse(Call<List<PotterCharacter>> call, Response<List<PotterCharacter>> response) {

                potters.postValue(response.body());
            }

            @Override
            public void onFailure(Call<List<PotterCharacter>> call, Throwable t) {
                Log.i("listCharacters","Failure on getting list characters from service");
            }
        });
        return potters;
    }

    public LiveData<String> getSortingHat() {
        Call<String> call= potterService.getSortingHat();
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                String houseResponse = response.body();
                house.postValue(houseResponse);
                //Toast.makeText(application.getApplicationContext(), house, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
        return house;
    }
}
