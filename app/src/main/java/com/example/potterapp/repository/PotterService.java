package com.example.potterapp.repository;

import com.example.potterapp.model.PotterCharacter;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface PotterService {

   @GET("/v1/characters")
   Call<List<PotterCharacter>> listCharacters(@Query("key")String key);

   @GET("/v1/sortingHat")
    Call<String> getSortingHat();
}
