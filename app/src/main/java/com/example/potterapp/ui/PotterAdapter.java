package com.example.potterapp.ui;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.potterapp.R;
import com.example.potterapp.model.PotterCharacter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

class PotterAdapter extends RecyclerView.Adapter<PotterAdapter.PotterHolder>{

    List<PotterCharacter> potterCharacters = new ArrayList<PotterCharacter>();

    public void setPotterCharacters(List<PotterCharacter> potterCharacters) {
        this.potterCharacters = potterCharacters;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public PotterHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.pottercharc_item,parent, false);
        return new PotterHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PotterHolder holder, int position) {
        PotterCharacter potterCharacter= potterCharacters.get(position);
        holder.nameCharacter.setText(potterCharacter.getName());
    }

    @Override
    public int getItemCount() {
        return potterCharacters.size();
    }

    public class PotterHolder extends RecyclerView.ViewHolder {

       @BindView(R.id.nameCharacter)
        TextView nameCharacter;

        public PotterHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

    }
}
