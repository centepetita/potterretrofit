package com.example.potterapp.ui;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.example.potterapp.model.PotterCharacter;
import com.example.potterapp.repository.PotterRepository;

import java.util.List;

public class PotterCharacterListViewModel extends AndroidViewModel {
    PotterRepository repository;

    public PotterCharacterListViewModel(@NonNull Application application) {
        super(application);
        repository = new PotterRepository(application);
    }


    public LiveData<List<PotterCharacter>> getPotterCharacters() {
        return repository.getPotterCharacters();
    }
}
