package com.example.potterapp.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.potterapp.R;
import com.example.potterapp.model.PotterCharacter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PotterCharacterListFragment extends Fragment {

    @BindView(R.id.potterCharactRecyclerV)
    RecyclerView potterCharactRecyclerV;
    private PotterCharacterListViewModel mViewModel;
    PotterAdapter adapter;

    public static PotterCharacterListFragment newInstance() {
        return new PotterCharacterListFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.potter_character_list_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(PotterCharacterListViewModel.class);
         adapter= new PotterAdapter();
        mViewModel.getPotterCharacters().observe(this, this::onPotterCharactersChange);
        potterCharactRecyclerV.setAdapter(adapter);
    }

    private void onPotterCharactersChange(List<PotterCharacter> potterCharacters) {
        adapter.setPotterCharacters(potterCharacters);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        potterCharactRecyclerV.setHasFixedSize(true);
        potterCharactRecyclerV.setLayoutManager(new LinearLayoutManager(this.getContext()));

    }
}
